# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import serial
import tkinter as tk
import connection as con
from tkinter import Scale
from tkinter import HORIZONTAL
from tkinter import BOTTOM
from tkinter import X
from tkinter import Label
from tkinter import SUNKEN
from tkinter import W

conn = con.LeonardoConnection()

def resetAurdino():
    for i in range(3):
        conn.write(bytes([0]))

def prepAurdino(pin):
    conn.write(bytes([115]))
    conn.write(bytes([pin]))

def trebbleOrBase(val,pin):
    resetAurdino()
    prepAurdino(pin)
    conn.write(bytes([int(254-(val*(254.0/100.0)))]))
    
def trebble(t):
    v=trebbleSlider.get()
    trebbleOrBase(v,6)
    
def bass(b):
    v=bassSlider.get()
    trebbleOrBase(v,10)
    
def resetSliders():
    bassSlider.set(100)
    trebbleSlider.set(100)
    
def resetBassAndTrebble():
    trebbleOrBase(100,10)
    trebbleOrBase(100,6)
    
    

def arduino_connection(a):
    if a==1:
        conn.connect(str(realVar.get()))
        if conn.is_connected() != True:
            return 0
        else:
            return 1
    
    else:
        conn.disconnect()
        if conn.is_connected() !=a:
            return 0
        else:
            return 1
        
        
def com_selection():
    return realVar.get()

    

#windo setup:
window = tk.Tk()
window.title("19814003")

mframe = tk.Frame(window)
sframe = tk.Frame(window)


#Button functionality:
def update_btn_text():
    if conn.is_connected()==0:
        if arduino_connection(1):
            button_text.set("Disconnect")
            connectionStatus.set("Status: Connected")
            resetSliders()
            resetBassAndTrebble()
            
    elif arduino_connection(0):
        button_text.set("Connect")
        connectionStatus.set("Status: Disconected")

button_text = tk.StringVar()
connectionStatus = tk.StringVar()
button_text.set("Connect")
connectionStatus.set("Status: Disconected")



#widgets:
connectButton = tk.Button(mframe,textvariable=button_text,command=lambda: update_btn_text(),width=15)
trebbleSlider = Scale(mframe,from_=0,to=100,orient=HORIZONTAL,command=trebble)
bassSlider = Scale(mframe,from_=0,to=100,orient=HORIZONTAL,command=bass)
resetSliders()
real = serial.tools.list_ports_windows.comports()
realVar = tk.StringVar()
realVar.set(None)
for i in range(len(real)):
    comChoice = tk.Radiobutton(window, variable = realVar,value=str(real[i])[:4], text= real[i],command=com_selection).pack()



#labels:
trebbleLabel = tk.Label(mframe,text="Trebble (pin 6)")
bassLabel = tk.Label(mframe,text="Bass (pin 10)")
statusBar = Label(sframe,textvariable=connectionStatus,bd=2,relief=SUNKEN,anchor=W)


#placement of widgets:
connectButton.grid(row=0,column=1)

trebbleSlider.grid(row=1,column=0)
trebbleLabel.grid(row=2,column=0)

bassSlider.grid(row=1,column=2)
bassLabel.grid(row=2,column=2)

mframe.pack()
statusBar.pack(side=BOTTOM,fill=X)
sframe.pack(side=BOTTOM,fill=X)

update_btn_text()
    
window.mainloop()